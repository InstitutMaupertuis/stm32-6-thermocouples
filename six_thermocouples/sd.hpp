#ifndef SD_HPP
#define SD_HPP

#include <SPI.h>
#include <SD.h>

String getNextFileName()
{
  unsigned i(0);
  // May lead to problems if creating zillion files...
  while (1)
  {
    String file_name = String(i++);
    file_name += ".csv";
    if (!SD.exists(file_name))
      return file_name;
  }
}

#endif
