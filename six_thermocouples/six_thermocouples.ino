#include <SD.h>
#include "sd.hpp"
#include "Adafruit_SSD1306_STM32.hpp"
#include "MAX31855.hpp"

// Temperatures // SPI
double *temperatures;
#define TH_1_CS PA0
#define TH_2_CS PA1
#define TH_3_CS PA2
#define TH_4_CS PA3
#define TH_5_CS PA4
#define TH_6_CS PB10
// Uses hardware SPI
MAX31855 th_1(TH_1_CS);
MAX31855 th_2(TH_2_CS);
MAX31855 th_3(TH_3_CS);
MAX31855 th_4(TH_4_CS);
MAX31855 th_5(TH_5_CS);
MAX31855 th_6(TH_6_CS);
const uint32_t thermocouple_fetch_period(100000); // 100 milliseconds

// Record trigger
#define BUTTON PB4
#define PC817 PB5
bool record_triggered = false;
bool recording = false;
uint64_t record_start_ms = 0;

// OLED screen // I2C
#define OLED_RESET PA4
Adafruit_SSD1306 display(OLED_RESET);
#if (SSD1306_LCDHEIGHT != 64)
#error("Height incorrect, please fix Adafruit_SSD1306.h!");
#endif
const uint32_t oled_display_period(250000); // 250 milliseconds

// SD card
String file_name("");
#define SD_CS PB11
File sd_file;
bool no_sd(false);

void initOLED()
{
  display.begin(SSD1306_SWITCHCAPVCC, 0x3C);
  display.clearDisplay();
  display.setTextSize(1);
  display.setTextColor(WHITE);
}

void displayTemperatures()
{
  display.clearDisplay();
  unsigned int y_pos = 0;

  // Print temperatures
  for (unsigned i = 0; i < 6; ++i)
  {
    display.setCursor(0, y_pos);
    display.print("T = ");
    if (isnan(temperatures[i]))
      display.print("NaN");
    else
      display.println(temperatures[i]);
    y_pos += 11;
  }

  if (recording)
  {
    if (no_sd)
    {
      display.setCursor(80, 30);
      display.print("NO SD!");
    }
    else
    {
      display.drawCircle(110, 10, 10, WHITE);
      display.fillCircle(110, 10, 5, WHITE);
      display.setCursor(80, 30);
      display.print(file_name);
    }
  }

  display.display();
}

void startRecording()
{
  static unsigned i(0);
  if (recording)
    return;

  digitalWrite(LED_BUILTIN, LOW); // On

  file_name = getNextFileName();
  sd_file = SD.open(file_name, FILE_WRITE);
  if (!sd_file)
    no_sd = true;
  else
   no_sd = false;
  
  recording = true;
  sd_file.println("time (msec); t1; t2; t3; t4; t5; t6");
  record_start_ms = millis();
}

void stopRecording()
{
  if (!recording)
    return;

  digitalWrite(LED_BUILTIN, HIGH); // Off
  sd_file.close();
  recording = false;
}

void record_isr()
{
  record_triggered = true;
}

void setup()
{
  Serial.begin(115200);

  pinMode(LED_BUILTIN, OUTPUT);
  digitalWrite(LED_BUILTIN, HIGH); // Off
  initOLED();

  // SD Card SPI CS
  pinMode(SD_CS, OUTPUT);
  digitalWrite(SD_CS, HIGH);
  SD.begin(SD_CS);

  // Triggers: PC817 and button
  pinMode(BUTTON, INPUT_PULLUP);
  pinMode(PC817, INPUT_PULLUP);
  attachInterrupt(BUTTON, record_isr, RISING);
  attachInterrupt(PC817, record_isr, RISING);
  stopRecording();

  temperatures = new double[6];
  temperatures[0] = 0;
  temperatures[1] = 0;
  temperatures[2] = 0;
  temperatures[3] = 0;
  temperatures[4] = 0;
  temperatures[5] = 0;
}

unsigned count(0);
uint64_t last(0);
uint32_t fetch_th(0);
uint32_t display_t(0);
uint32_t record_trigger_timer(200000);
void loop()
{
  // Count time (usec) elapsed since last time
  uint64_t right_now(micros());
  uint64_t usec_elapsed(right_now - last);
  if (last == 0)
    usec_elapsed = 0;
  last = right_now;

  if (record_trigger_timer <= usec_elapsed)
  {
    if (record_triggered)
    {
      record_triggered = false;
      if (recording)
        stopRecording();
      else
        startRecording();
      record_trigger_timer = 200000;
      display_t = oled_display_period; // Force refresh OLED
    }
  }
  else
  {
    record_trigger_timer -= usec_elapsed;
    record_triggered = false;
  }

  fetch_th += usec_elapsed;
  if (fetch_th >= thermocouple_fetch_period)
  {
    fetch_th -= thermocouple_fetch_period;

    // Fetch temperatures, try twice because
    // writing the SD card (SPI) sometimes corrupts 
    // the thermocouple data.
    if (!th_1.readCelsius(temperatures[0]))
      th_1.readCelsius(temperatures[0]);
      
    if (!th_2.readCelsius(temperatures[1]))
      th_2.readCelsius(temperatures[1]);
      
    if (!th_3.readCelsius(temperatures[2]))
      th_3.readCelsius(temperatures[2]);
      
    if (!th_4.readCelsius(temperatures[3]))
      th_4.readCelsius(temperatures[3]);
      
    if (!th_5.readCelsius(temperatures[4]))
      th_5.readCelsius(temperatures[4]);
      
    if (!th_6.readCelsius(temperatures[5]))
      th_6.readCelsius(temperatures[5]);

    // Record temperatures in SD card
    if (recording & !no_sd)
    {
      sd_file.print(millis() - record_start_ms);
      sd_file.print("; ");
      for (unsigned i(0); i < 5; ++i)
      {
        if (isnan(temperatures[i]))
          sd_file.print("NaN");  
        else
          sd_file.print(temperatures[i]);
        sd_file.print("; ");
      }
      if (isnan(temperatures[5]))
        sd_file.println("NaN");  
      else
        sd_file.println(temperatures[5]);

       delayMicroseconds(40);
    }
    
    // Send temperatures over serial
    for (unsigned i(0); i < 5; ++i)
    {
      if (isnan(temperatures[i]))
        Serial.print("NaN");  
      else
        Serial.print(temperatures[i]);
      Serial.print("; ");
    }
    if (isnan(temperatures[5]))
      Serial.println("NaN");  
    else
      Serial.println(temperatures[5]);
  }

  // Display temperatures with OLED
  display_t += usec_elapsed;
  if (display_t >= oled_display_period)
  {
    display_t -= oled_display_period;
    displayTemperatures();
  }
}
