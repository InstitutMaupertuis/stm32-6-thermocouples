/***************************************************
  This is a library for the Adafruit Thermocouple Sensor w/MAX31855K

  Designed specifically to work with the Adafruit Thermocouple Sensor
  ----> https://www.adafruit.com/products/269

  These displays use SPI to communicate, 3 pins are required to
  interface
  Adafruit invests time and resources providing this open source code,
  please support Adafruit and open-source hardware by purchasing
  products from Adafruit!

  Written by Limor Fried/Ladyada for Adafruit Industries.
  BSD license, all text above must be included in any redistribution
 ****************************************************/

#ifndef MAX31855_HPP
#define MAX31855_HPP

#if (ARDUINO >= 100)
 #include "Arduino.h"
#else
 #include "WProgram.h"
#endif

class MAX31855 {
 public:
  MAX31855(int8_t _cs);

  void begin(void);
  double readInternal(void);
  bool readCelsius(double &t);
  uint8_t readError();

 private:
  boolean initialized;

  int8_t cs;
  uint32_t spiread32(void);
};

#endif
