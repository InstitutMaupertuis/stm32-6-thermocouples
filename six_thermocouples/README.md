# Bootloader
Compiler et installer [texane/stlink](https://github.com/texane/stlink.git)

Télécharger et flasher le bootloader avec un ST-Link V2:
```bash
git clone https://github.com/rogerclarkmelbourne/STM32duino-bootloader.git
sudo st-flash write ./STM32duino-bootloader/binaries/generic_boot20_pc13.bin 0x8000000
```

# Arduino
- Installer la dernière version du logiciel Arduino (minimum 1.8.8)
- [Installation stm32duino](https://gitlab.com/VictorLamoine/docker/blob/master/arduino:stm32/setup_script.bash)
- Installer `Arduino GFX Library` via le gestionnaire de bibliothèques du logiciel Arduino (ou `arduino --install-library "Adafruit GFX Library"`)

Compiler le programme et l'uploader via le port série sur l'Arduino. Appuyer sur le bouton reset puis uploader immédiatement si l'upload échoue.
