/***************************************************
  This is a library for the Adafruit Thermocouple Sensor w/MAX31855K

  Designed specifically to work with the Adafruit Thermocouple Sensor
  ----> https://www.adafruit.com/products/269

  These displays use SPI to communicate, 3 pins are required to
  interface
  Adafruit invests time and resources providing this open source code,
  please support Adafruit and open-source hardware by purchasing
  products from Adafruit!

  Written by Limor Fried/Ladyada for Adafruit Industries.
  BSD license, all text above must be included in any redistribution
 ****************************************************/

#include "MAX31855.hpp"
#include <stdlib.h>
#include <SPI.h>

MAX31855::MAX31855(int8_t _cs) {
  cs = _cs;
  initialized = false;
}

void MAX31855::begin(void) {
  //define pin modes
  pinMode(cs, OUTPUT);
  digitalWrite(cs, HIGH);

  // hardware SPI
  //start and configure hardware SPI
  SPI.begin();
  initialized = true;
}

double MAX31855::readInternal(void) {
  uint32_t v;
  v = spiread32();

  // ignore bottom 4 bits - they're just thermocouple data
  v >>= 4;

  // pull the bottom 11 bits off
  float internal = v & 0x7FF;
  // check sign bit!
  if (v & 0x800) {
    // Convert to negative value by extending sign and casting to signed type.
    int16_t tmp = 0xF800 | (v & 0x7FF);
    internal = tmp;
  }
  internal *= 0.0625; // LSB = 0.0625 degrees
  //Serial.print("\tInternal Temp: "); Serial.println(internal);
  return internal;
}

bool MAX31855::readCelsius(double &t) {

  int32_t v;
  v = spiread32();

  if (v & 0x7) {
    // uh oh, a serious problem!
    t = NAN;
    return false;
  }


  if (v & 0x80000000) {
    // Negative value, drop the lower 18 bits and explicitly extend sign bits.
    v = 0xFFFFC000 | ((v >> 18) & 0x00003FFFF);
  }
  else {
    // Positive value, just drop the lower 18 bits.
    v >>= 18;
  }

  double centigrade = v;
  centigrade *= 0.25;

  if (centigrade < -800)
  {
    t = NAN;
    return false;    
  }

  // LSB = 0.25 degrees C
  t = centigrade; 
  return true;
}

uint8_t MAX31855::readError() {
  return spiread32() & 0x7;
}

uint32_t MAX31855::spiread32(void) {
  int i;
  uint32_t d = 0;

  if (!initialized)
    begin();

  digitalWrite(cs, LOW);
  d = SPI.transfer(0);
  d <<= 8;
  d |= SPI.transfer(0);
  d <<= 8;
  d |= SPI.transfer(0);
  d <<= 8;
  d |= SPI.transfer(0);
  digitalWrite(cs, HIGH);

  return d;
}
