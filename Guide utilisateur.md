# Carte mesure 6 thermocouples - Guide utilisateur

![PCB](./pictures/PCB.jpg)

Cette carte permet de:
- Mesurer 6 températures
- Afficher les températures mesurées à l'écran
- Déclencher un enregistrement sur carte micro SD via un bouton ou une source externe 24V
- Récupérer les températures en direct via le port série sur le micro USB du microcontrôleur

![Affichage](./pictures/display.jpg)

# Alimentation
La carte doit être alimentée en 5V via le port micro USB pour fonctionner. On peut utiliser une batterie portable ("powerbank") pour l'alimenter.

## Thermocouples et connecteurs
- Thermocouples type K uniquement
- Le fil vert doit aller sur la gauche du connecteur

![Connecteur](./pictures/connector.jpg)

- Si l'écran affiche `NaN` (not a number) pour un thermocouple vérifier la connexion du thermocouple, si le thermocouple est bien connecté essayer avec un autre thermocouple
- Pour être sûr qu'un thermocouple est bien branché le faire chauffer et vérifier que la température augmente sur l'écran (si la température diminue le thermocouple est branché à l'envers)

## Déclenchement externe
Le déclenchement se fait via un signal 24V, le signal est isolé avec un photocoupleur.

Polarité du connecteur:

![Connecteur](./pictures/connector.jpg)

- Vert: 24V
- Blanc: Masse

Le déclenchement s'active sur un front montant. Un premier front montant déclenche l'enregistrement, un deuxième l'arrête.

## Déclenchement avec le bouton
Un appui sur le bouton démarre / arrête l'enregistrement. Un premier appui déclenche l'enregistrement, un deuxième l'arrête.

## Carte micro SD
Ne pas déconnecter ou connecter la carte SD lorsque la carte est sous tension!

- La carte doit être formatée en `FAT32` pour que l'enregistrement fonctionne
- Les fichiers enregistrés sont numérotés dans un ordre croissant (`0.CSV`, `1.CSV` etc.)
- Le premier nombre libre est utilisé comme nom de fichier

## Enregistrement
Les 6 températures sont enregistrées 10x par seconde (10 Hz).

Lorsque l'enregistrement démarre un point s'affiche en haut à droite de l'écran et le nom du fichier au milieu à droite:

![Affichage enregistrement](./pictures/display_record.jpg)

Dans le cas ou le message `NO SD!` apparaît la carte SD est:
- Pas insérée
- Mal / pas formatée en `FAT32`
- Pleine

![Affichage erreur enregistrement](./pictures/display_record_error.jpg)

## Format des fichiers CSV
Les fichiers enregistrés sont des fichiers texte `CSV`:

```
time (msec); t1; t2; t3; t4; t5; t6
33; 23.50; NaN; NaN; NaN; NaN; NaN
133; 25.50; NaN; NaN; NaN; NaN; NaN
```

- Les valeurs sont séparées par des `;`, suivant le logiciel utilisé (LibreOffice Calc, Excel) il faut remplacer tous les `.` par des `,` pour pouvoir tracer les graphes de températures
- `NaN` signifie que la valeur n'a pas pu être enregistrée (thermocouple perturbé)

## Port série sur USB
Il est possible de récupérer les valeurs des températures et enregistrer les données via le port USB.

Installer le logiciel [ArduinoScope](https://gitlab.com/VictorLamoine/ArduinoScope), le lancer et configurer le baud-rate à `115200`.

Configurer le nombre de graphes à 6 (même si on utilise pas tous les thermocouples).

![ArduinoScope](./pictures/ArduinoScope.png)
