[![Institut Maupertuis logo](http://www.institutmaupertuis.fr/media/gabarit/logo.png)](http://www.institutmaupertuis.fr)

![PCB](./pictures/PCB.jpg)

![CAD](./pictures/CAD.png)

# Informations
- [Conception électronique (EDA)](./EDA): KiCAD 5.0.2
- [Conception mécanique](./CAD): FreeCAD 0.18 dev
- [Code Arduino pour STM32](./six_thermocouples)

# Guide d'utilisation
- [Guide utilisateur](./Guide utilisateur.md)

# ROS node
Pour publier les valeurs sur des `topics` ROS: 
https://gitlab.com/InstitutMaupertuis/serial_temperatures